# SYCL: https://github.com/intel/llvm/issues/6636
# https://support.codeplay.com/t/cmake-integration-for-oneapi-for-nvidia-gpus/542/7
CXXOPT := -std=c++20 -O3 -Wall
SYCLONDEVICE := -fsycl -fsycl-targets=nvptx64-nvidia-cuda
ICPX_FLAGS := -DCOMPILER_NAME=\"intel\"
CLANG_FLAGS := -DCOMPILER_NAME=\"clang\"
ACPP_FLAGS := --acpp-targets="generic" -DCOMPILER_NAME=\"acpp\"

# user can specify custom clang++ path
# then call for instance:
# make PATH_TO_LLVM=/home/ivan/llvm/build/install COMPILER=clang++ join
PATH_TO_COMPILER := /usr

CXX :=
CXXFLAGS := 

ifeq ($(COMPILER),icpx)
	CXX := icpx
	CXXFLAGS := $(SYCLONDEVICE) $(ICPX_FLAGS)
endif

ifeq ($(COMPILER),clang++)
	CXX := $(PATH_TO_COMPILER)/bin/clang++
	CXXFLAGS := $(SYCLONDEVICE) $(CLANG_FLAGS)
	LD_LIBRARY_PATH := $(PATH_TO_COMPILER)/lib
	export LD_LIBRARY_PATH
endif

ifeq ($(COMPILER),acpp)
	CXX := $(PATH_TO_COMPILER)/bin/acpp
	CXXFLAGS := $(ACPP_FLAGS)
endif

# Default target
all:

# Pattern rule to compile a file
%: %.cpp
	$(CXX) $(CXXFLAGS) $(CXXOPT)  $< -o $@