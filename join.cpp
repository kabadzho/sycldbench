#include <iostream>
#include <numeric>
#include <string>
#include <sycl/sycl.hpp>
#include <vector>

#include <fstream>

#include "block_helpers/constants.hpp"
#include "block_helpers/join.hpp"
#include "block_helpers/load.hpp"

#include "inc/generator.hpp"
#include "inc/helpers.hpp"

#include <getopt.h>

template <int BLOCK_THREADS, int ITEMS_PER_THREAD>
void probe(int *probe_key, long long num_tuples, int *hash_table,
           long long num_slots, sum_reduction_t<unsigned long long> &sum,
           const sycl::nd_item<1> &item) {
  // Load a tile striped across threads
  int items[ITEMS_PER_THREAD];
  int items2[ITEMS_PER_THREAD];
  bool selection_flags[ITEMS_PER_THREAD];

  const long long tile_offset = item.get_group(0) * TILE_ITEMS;
  const long long num_tiles = (num_tuples + TILE_ITEMS - 1) / TILE_ITEMS;
  const long long num_tile_items = (item.get_group(0) == num_tiles - 1)
                                       ? num_tuples - tile_offset
                                       : TILE_ITEMS;

  // no filter, so all items are selected initially
  set_selection_flags<BLOCK_THREADS, ITEMS_PER_THREAD>(
      item.get_local_id(0), selection_flags, num_tile_items);

  block_load<int, BLOCK_THREADS, ITEMS_PER_THREAD>(
      item.get_local_id(0), tile_offset, probe_key, items, num_tile_items);
  probe_keys_vals<int, int, BLOCK_THREADS, ITEMS_PER_THREAD>(
      item.get_local_id(0), items, items2, selection_flags, hash_table,
      num_slots, 0, num_tile_items);

#pragma unroll
  for (int i = 0; i < ITEMS_PER_THREAD; ++i) {
    if (item.get_local_id(0) + (BLOCK_THREADS * i) < num_tile_items) {
      if (selection_flags[i]) {
        sum.combine(items[i] * items2[i]);
      }
    }
  }
}

template <int BLOCK_THREADS, int ITEMS_PER_THREAD>
void build(int *build_key, int *build_val, long long num_tuples,
           int *hash_table, long long num_slots, const sycl::nd_item<1> &item) {
  // Load a tile striped across threads
  int items[ITEMS_PER_THREAD];
  int items2[ITEMS_PER_THREAD];
  bool selection_flags[ITEMS_PER_THREAD];

  const long long tile_offset = item.get_group(0) * TILE_ITEMS;
  const long long num_tiles = (num_tuples + TILE_ITEMS - 1) / TILE_ITEMS;
  const long long num_tile_items = (item.get_group(0) == num_tiles - 1)
                                       ? num_tuples - tile_offset
                                       : TILE_ITEMS;

  // no filter, so all items are selected initially
  set_selection_flags<BLOCK_THREADS, ITEMS_PER_THREAD>(
      item.get_local_id(0), selection_flags, num_tile_items);

  block_load<int, BLOCK_THREADS, ITEMS_PER_THREAD>(
      item.get_local_id(0), tile_offset, build_key, items, num_tile_items);
  block_load<int, BLOCK_THREADS, ITEMS_PER_THREAD>(
      item.get_local_id(0), tile_offset, build_val, items2, num_tile_items);
  build_keys_vals<int, int, BLOCK_THREADS, ITEMS_PER_THREAD>(
      item.get_local_id(0), items, items2, selection_flags, hash_table,
      num_slots, 0, num_tile_items);
}

int main(int argc, char **argv) {
  long long build_key_len = 1 << 28; // 24
  long long probe_key_len = 1 << 28; // 28
  int c;
  while ((c = getopt(argc, argv, "b:p:")) != -1) {
    switch (c) {
    case 'b':
      build_key_len = atoll(optarg);
      break;
    case 'p':
      probe_key_len = atoll(optarg);
      break;
    }
  }

  std::cout << "Build size: " << build_key_len * 2 / 1024 / 1024 * sizeof(int)
            << " MB" << std::endl;
  std::cout << "Probe size: " << probe_key_len / 1024 / 1024 * sizeof(int)
            << " MB" << std::endl;

  sycl::queue queue{
      sycl::default_selector_v,
      sycl::property_list{sycl::property::queue::enable_profiling()}};

  sycl::queue cpu_queue{sycl::cpu_selector_v};

  auto d = queue.get_device();
  size_t totalMemory = d.get_info<sycl::info::device::global_mem_size>();
  std::cout << "Total Device Mem (GB): " << totalMemory / 1024 / 1024 / 1024
            << std::endl;

  auto device_name = queue.get_device().get_info<sycl::info::device::name>();
  std::cout << "Running on " << device_name << "\n";

  bool copying = queue.get_device() != cpu_queue.get_device();

  int *h_build_key;
  int *h_build_val;
  int *h_probe_key;
  std::cout << "Generating data..." << std::endl;
  create_relation_build(h_build_key, h_build_val, build_key_len, cpu_queue);
  create_relation_probe(h_probe_key, probe_key_len, build_key_len, cpu_queue);
  std::cout << "Data generated." << std::endl;

  int *hash_table = sycl::malloc_device<int>(build_key_len * 2, queue);
  int *build_key = sycl::malloc_device<int>(build_key_len, queue);
  int *build_val = sycl::malloc_device<int>(build_key_len, queue);
  int *probe_key = sycl::malloc_device<int>(probe_key_len, queue);

  unsigned long long res = 0;
  sycl::buffer<unsigned long long> res_b(&res, 1);

  sycl::range<1> gws_build(GWS(build_key_len));
  sycl::range<1> gws_probe(GWS(probe_key_len));
  sycl::range<1> lws(N_BLOCK_THREADS);

  sycl::event event{};
  std::vector<float> times_build(REPEAT, 0.0f);
  std::vector<float> times_probe(REPEAT, 0.0f);
  std::vector<float> times_transfer_probe(REPEAT, 0.0f);

  sycl::buffer<bool> sf_probe((sycl::range<1>(probe_key_len)));
  sycl::buffer<int> probe_vals((sycl::range<1>(probe_key_len)));

  for (int i = 0; i < REPEAT; i++) {

    if (copying) {
      queue.memcpy(build_key, h_build_key, build_key_len * sizeof(int)).wait();
      queue.memcpy(build_val, h_build_val, build_key_len * sizeof(int)).wait();
      event = queue.memcpy(probe_key, h_probe_key, probe_key_len * sizeof(int));
      times_transfer_probe[i] += wait_and_measure_time(event);
    } else {
      build_key = h_build_key;
      build_val = h_build_val;
      probe_key = h_probe_key;
    }

    event = queue.submit([&](sycl::handler &cgh) {
      cgh.parallel_for(sycl::nd_range<1>(gws_build, lws),
                       [=](sycl::nd_item<1> item) {
                         build<N_BLOCK_THREADS, N_ITEMS_PER_THREAD>(
                             build_key, build_val, build_key_len, hash_table,
                             build_key_len, item);
                       });
    });
    times_build[i] = wait_and_measure_time(event);
    event = queue.submit([&](sycl::handler &cgh) {
      auto sum_reduction =
          sycl::reduction(res_b, cgh, std::plus<unsigned long long>());
      cgh.parallel_for(
          sycl::nd_range<1>(gws_probe, lws), sum_reduction,
          [=](sycl::nd_item<1> item, sum_reduction_t<unsigned long long> &sum) {
            probe<N_BLOCK_THREADS, N_ITEMS_PER_THREAD>(
                probe_key, probe_key_len, hash_table, build_key_len, sum, item);
          });
    });
    times_probe[i] = wait_and_measure_time(event);
    res = res_b.get_host_access()[0];
    std::cout << "Result: " << res << std::endl;
    res = 0;
  }
  if (copying) {
    std::cout << "Transfer probe times:" << std::endl;
    print_mean_and_variance_of_time_cold_hot(times_transfer_probe);
  }
  std::cout << "\nBuild times:" << std::endl;
  auto build_stats = print_mean_and_variance_of_time_cold_hot(times_build);
  std::cout << "Probe times:" << std::endl;
  auto stats_res = print_mean_and_variance_of_time_cold_hot(times_probe);

  std::cout << "B,0," << build_key_len << "," << REPEAT
            << "," << build_stats.cold << "," << build_stats.avg_hot << ","
            << build_stats.best_hot << "," << build_stats.stddev_hot << ","
            << COMPILER_NAME << "," << device_name << std::endl;

  std::cout << "J," << build_key_len << "," << probe_key_len << "," << REPEAT
            << "," << stats_res.cold << "," << stats_res.avg_hot << ","
            << stats_res.best_hot << "," << stats_res.stddev_hot << ","
            << COMPILER_NAME << "," << device_name << std::endl;

  // dump the benchmark result to a file:
  std::ofstream file;
  file.open("output.csv", std::ios_base::app);
  file << "B,0," << build_key_len << "," << REPEAT
       << "," << build_stats.cold << "," << build_stats.avg_hot << ","
       << build_stats.best_hot << "," << build_stats.stddev_hot << ","
       << COMPILER_NAME << "," << device_name << std::endl;
  file << "J," << build_key_len << "," << probe_key_len << "," << REPEAT
       << "," << stats_res.cold << "," << stats_res.avg_hot << ","
       << stats_res.best_hot << "," << stats_res.stddev_hot << ","
       << COMPILER_NAME << "," << device_name << std::endl;
  file.close();

  return 0;
}