#pragma once

// given:
// select sum(probe.col3),build2.vals from build1, build2, probe,
// where probe.col1 = build1.keys and probe.col2 = build2.keys
// group by build2.vals;
//
// would correspond to a total of 5 kernel launches:
// first for build1, then for build2, and finally for probe
// 1 for build1: suffices to call build_keys_only
// 1 for build2: call build_keys_vals
// 3 for probe:
// 1. probe_keys_only on build1
// 2. probe_keys_vals on build2
// 3. aggregate the results using the lookup values from build2
// i.e., the final aggregation step would be similar to:
// >> if (selection_flags[i]) {
// >>   int hash = HASH(build2.vals[i], ht_len, vals_min);
// >>   res[hash * 2] = build2.vals[i];
// >>   res[hash * 2 + 1] += probe.col3; }

#define HASH(X, Y, Z) ((X - Z) % Y)

// https://github.com/zjin-lcf/HeCBench/blob/master/src/graphB%2B-sycl/kernels.h#L4-L13
// atomic compare and swap with no dpcpp dependency
template <typename K> inline void atomicCAS(K *val, K expected, K desired) {
  auto atm = sycl::atomic_ref<K, sycl::memory_order::relaxed,
                              sycl::memory_scope::device,
                              sycl::access::address_space::global_space>(*val);
  atm.compare_exchange_strong(expected, desired);
}

// omitting the rows which did not pass earlier selections
// simply shifts the column to now start with its minimum value
template <typename K, int BLOCK_THREADS, int ITEMS_PER_THREAD>
inline void build_keys_only(long long tid, K (&keys)[ITEMS_PER_THREAD],
                            bool (&sf)[ITEMS_PER_THREAD], K *ht, long long ht_len,
                            K keys_min, long long num_items) {
  // if not in the last block, no need to check for out of bounds
  if ((BLOCK_THREADS * ITEMS_PER_THREAD) == num_items) {
    for (int i = 0; i < ITEMS_PER_THREAD; i++) {
      if (sf[i]) {
        atomicCAS<K>(&ht[HASH(keys[i], ht_len, keys_min)], 0, keys[i]);
        //ht[HASH(keys[i], ht_len, keys_min)] = keys[i];
      }
    }
  } else {
    for (int i = 0; i < ITEMS_PER_THREAD; i++) {
      if (tid + (i * BLOCK_THREADS) < num_items) {
        if (sf[i]) {
          atomicCAS<K>(&ht[HASH(keys[i], ht_len, keys_min)], 0, keys[i]);
          //ht[HASH(keys[i], ht_len, keys_min)] = keys[i];
        }
      }
    }
  }
}

// omitting the rows which did not pass earlier selections
// first, shifts the key column to now start with its minimum value
// then store the keys in odd slots and the values in even slots
template <typename K, typename V, int BLOCK_THREADS, int ITEMS_PER_THREAD>
inline void build_keys_vals(long long tid, K (&keys)[ITEMS_PER_THREAD],
                            V (&vals)[ITEMS_PER_THREAD],
                            bool (&sf)[ITEMS_PER_THREAD], K *ht, long long ht_len,
                            K keys_min, long long num_items) {
  // if not in the last block, no need to check for out of bounds
  if ((BLOCK_THREADS * ITEMS_PER_THREAD) == num_items) {
    for (int i = 0; i < ITEMS_PER_THREAD; i++) {
      if (sf[i]) {
        long long hash = HASH(keys[i], ht_len, keys_min);
        atomicCAS<K>(&ht[hash << 1], 0, keys[i]);
        //ht[hash << 1] = keys[i];
        ht[(hash << 1) + 1] = vals[i];
      }
    }
  } else {
    for (int i = 0; i < ITEMS_PER_THREAD; i++) {
      if (tid + (i * BLOCK_THREADS) < num_items) {
        if (sf[i]) {
          long long hash = HASH(keys[i], ht_len, keys_min);
          atomicCAS<K>(&ht[hash << 1], 0, keys[i]);
          //ht[hash << 1] = keys[i];
          ht[(hash << 1) + 1] = vals[i];
        }
      }
    }
  }
}

// update the selection flags of the probed table by build lookup
// a build with keys only is probed with probe keys only (<=>)
template <typename K, int BLOCK_THREADS, int ITEMS_PER_THREAD>
inline void probe_keys_only(long long tid, K (&keys)[ITEMS_PER_THREAD],
                            bool (&sf)[ITEMS_PER_THREAD], K *ht, long long ht_len,
                            K keys_min, long long num_items) {
  // if not in the last block, no need to check for out of bounds
  if ((BLOCK_THREADS * ITEMS_PER_THREAD) == num_items) {
    for (int i = 0; i < ITEMS_PER_THREAD; i++) {
      if (sf[i]) {
        sf[i] = ht[HASH(keys[i], ht_len, keys_min)];
      }
    }
  } else {
    for (int i = 0; i < ITEMS_PER_THREAD; i++) {
      if (tid + (i * BLOCK_THREADS) < num_items) {
        if (sf[i]) {
          sf[i] = ht[HASH(keys[i], ht_len, keys_min)];
        }
      }
    }
  }
}

// analogous update of selection flags for a probed table with keys and values
// temp_vals is now a newly created array to store the values of the build
template <typename K, typename V, int BLOCK_THREADS, int ITEMS_PER_THREAD>
inline void probe_keys_vals(long long tid, K (&keys)[ITEMS_PER_THREAD],
                            V (&temp_vals)[ITEMS_PER_THREAD],
                            bool (&sf)[ITEMS_PER_THREAD], K *ht, long long ht_len,
                            K keys_min, long long num_items) {
  // if not in the last block, no need to check for out of bounds
  if ((BLOCK_THREADS * ITEMS_PER_THREAD) == num_items) {
    for (int i = 0; i < ITEMS_PER_THREAD; i++) {
      if (sf[i]) {
        long long hash = HASH(keys[i], ht_len, keys_min);
        // obtain the key-value pair from a single lookup
        uint64_t slot = *reinterpret_cast<uint64_t *>(&ht[hash << 1]);
        if (slot != 0) {
          temp_vals[i] = (slot >> 32);
        } else {
          sf[i] = 0;
        }
      }
    }
  } else {
    for (int i = 0; i < ITEMS_PER_THREAD; i++) {
      if (tid + (i * BLOCK_THREADS) < num_items) {
        if (sf[i]) {
          long long hash = HASH(keys[i], ht_len, keys_min);
          uint64_t slot = *reinterpret_cast<uint64_t *>(&ht[hash << 1]);
          if (slot != 0) {
            temp_vals[i] = (slot >> 32);
          } else {
            sf[i] = 0;
          }
        }
      }
    }
  }
}

// there might be cases, in which all values are selected (i.e. no selection)
// need to manually set the selection flags to true
template <int BLOCK_THREADS, int ITEMS_PER_THREAD>
inline void set_selection_flags(long long tid, bool (&sf)[ITEMS_PER_THREAD],
                                long long num_items) {
  // if not in the last block, no need to check for out of bounds
  if ((BLOCK_THREADS * ITEMS_PER_THREAD) == num_items) {
    for (int i = 0; i < ITEMS_PER_THREAD; i++)
      sf[i] = true;
  } else {
    for (int i = 0; i < ITEMS_PER_THREAD; i++) {
      if (tid + (i * BLOCK_THREADS) < num_items)
        sf[i] = true;
    }
  }
}
