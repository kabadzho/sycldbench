#define N_BLOCK_THREADS 128
#define N_ITEMS_PER_THREAD 4

#define TILE_ITEMS (N_BLOCK_THREADS * N_ITEMS_PER_THREAD)

#define GWS(num_tuples)                                                        \
  ((num_tuples + TILE_ITEMS - 1) / TILE_ITEMS * N_BLOCK_THREADS)