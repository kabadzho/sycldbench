#include <sycl/sycl.hpp>

#pragma once

// the fundamental motivaion to load blocks is to avoid cache misses
// we use the memory access pattern introduced in crystal section 3.2
// https://anilshanbhag.in/static/papers/crystal_sigmod20.pdf

// all columns except for the selection flags are stored as raw arrays
// this allows for control over asynchronous transfers and kernel launches
template <typename T, int BLOCK_THREADS, int ITEMS_PER_THREAD>
inline void block_load(long long tid, long long tile_offset, T *col,
                       T (&local_col)[ITEMS_PER_THREAD], long long num_items) {
  // if not in the last block, no need to check for out of bounds
  if ((BLOCK_THREADS * ITEMS_PER_THREAD) == num_items) {
    for (int i = 0; i < ITEMS_PER_THREAD; i++)
      local_col[i] = col[tid + tile_offset + (i * BLOCK_THREADS)];
  } else {
    for (int i = 0; i < ITEMS_PER_THREAD; i++) {
      long long idx = tid + (i * BLOCK_THREADS);
      if (idx < num_items)
        local_col[i] = col[idx + tile_offset];
    }
  }
}

// the selection flags (sf) stay on the device, so benefit from accessors
// in case of fusion, buffers could be optimized, marking them local
// in addition, the temporary probed columns use buffers as well
template <typename T, int BLOCK_THREADS, int ITEMS_PER_THREAD>
inline void block_load(long long tid, long long tile_offset, sycl::accessor<T> sf,
                       T (&local_sf)[ITEMS_PER_THREAD], long long num_items) {
  // if not in the last block, no need to check for out of bounds
  if ((BLOCK_THREADS * ITEMS_PER_THREAD) == num_items) {
    for (int i = 0; i < ITEMS_PER_THREAD; i++)
      local_sf[i] = sf[tid + tile_offset + (i * BLOCK_THREADS)];
  } else {
    for (int i = 0; i < ITEMS_PER_THREAD; i++) {
      long long idx = tid + (i * BLOCK_THREADS);
      if (idx < num_items)
        local_sf[i] = sf[idx + tile_offset];
    }
  }
}

template <typename T, int BLOCK_THREADS, int ITEMS_PER_THREAD>
inline void block_store(long long tid, long long tile_offset, T *col,
                        T (&local_col)[ITEMS_PER_THREAD], long long num_items) {
  // if not in the last block, no need to check for out of bounds
  if ((BLOCK_THREADS * ITEMS_PER_THREAD) == num_items) {
    for (int i = 0; i < ITEMS_PER_THREAD; i++)
      col[tid + tile_offset + (i * BLOCK_THREADS)] = local_col[i];
  } else {
    for (int i = 0; i < ITEMS_PER_THREAD; i++) {
      long long idx = tid + (i * BLOCK_THREADS);
      if (idx < num_items)
        col[idx + tile_offset] = local_col[i];
    }
  }
}