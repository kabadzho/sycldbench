#include <math.h>
#include <sycl/sycl.hpp>

#define REPEAT 6

#ifndef COMPILER_NAME
#define COMPILER_NAME "undef" // intel, clang, acpp
#endif

template <typename R>
using sum_reduction_t = sycl::reducer<
    R, std::plus<R>, 0, 1UL,
    sycl::detail::ReductionIdentityContainer<R, std::plus<R>, false>>;

// https://github.com/intel/pti-gpu/blob/master/chapters/device_activity_tracing/DPCXX.md
float wait_and_measure_time(sycl::event e) {
  e.wait();
  const auto start =
      e.get_profiling_info<sycl::info::event_profiling::command_start>();
  const auto end =
      e.get_profiling_info<sycl::info::event_profiling::command_end>();
  float time = (end - start) / 1e6;
  return time;
}

struct stat_times {
  float cold;
  float avg_hot;
  float best_hot;
  float stddev_hot;
};

stat_times print_mean_and_variance_of_time_cold_hot(std::vector<float> ts_full) {
  auto ts = std::vector<float>(ts_full.begin() + 1, ts_full.end());
  float mean = std::accumulate(ts.begin(), ts.end(), 0.0) / ts.size();
  float variance = 0;
  for (auto t : ts) {
    //std::cout << "  time: " << t << " ms" << std::endl;
    variance += (t - mean) * (t - mean);
  }
  variance /= ts.size();
  variance = 100 * sqrt(variance) / mean;
  std::cout << "COLD: time: " << ts_full[0] << " ms" << std::endl;
  std::cout << "HOT: Avg time: " << mean << " ms"
            << std::endl;
  std::cout << "HOT: Best time: " << *std::min_element(ts.begin(), ts.end())
            << " ms" << std::endl;
  std::cout << "HOT: stddev: " << variance << " %" << std::endl;
  std::cout << std::endl;

  return {ts_full[0], mean, *std::min_element(ts.begin(), ts.end()), variance};
}

void print_and_reset_result(sycl::queue queue, unsigned long long *res) {
  std::cout << "  result: " << res[0] << std::endl;
  res[0] = 0;
}
