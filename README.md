# sycldbench

## Build and run:

> [!WARNING]
> We start with a small sized benchmarks and double the size of the input data in each iteration. Please report the results in `output.csv`

#### Option 1:

```
source /opt/intel/oneapi/setvars.sh
./runner.sh icpx
```

This will compile the benchmarks using `icpx` from intel oneAPI.
Then benchmarks will be run on the GPU device (specified in the Makefile).

#### Option 2:

```
./runner.sh clang++ /home/ivan/llvm/build/install
```
This will compile the benchmarks using `clang++` where `/home/ivan/llvm/build/install` is the path to an open source (nightly) build of `llvm` with `sycl` support.
Then benchmarks will be run on the GPU device (specified in the Makefile).

#### Option 3:

```
./runner.sh acpp /home/ivan/acpp/build/install
```
This will compile the benchmarks using `acpp` where `/home/ivan/acpp/build/install` is the path to the compiler.
The target device is the first device inside acpp-info output.

### Reporting results
