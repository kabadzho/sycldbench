#include <iostream>
#include <numeric>
#include <string>
#include <sycl/sycl.hpp>
#include <vector>

#include <fstream>

#include "block_helpers/constants.hpp"
#include "block_helpers/load.hpp"

#include "inc/generator.hpp"
#include "inc/helpers.hpp"

#include <getopt.h>

int main(int argc, char **argv) {
  long long proj_len = 1 << 28; // 28
  int c;
  while ((c = getopt(argc, argv, "p:")) != -1) {
    switch (c) {
    case 'p':
      proj_len = atoll(optarg);
      break;
    }
  }

  std::cout << "Project size: " << proj_len * 3 / 1024 / 1024 * sizeof(float)
            << " MB" << std::endl;

  sycl::queue queue{
      sycl::default_selector_v,
      sycl::property_list{sycl::property::queue::enable_profiling()}};

  sycl::queue cpu_queue{sycl::cpu_selector_v};

  size_t totalMemory =
      queue.get_device().get_info<sycl::info::device::global_mem_size>();
  // size_t freeMemory =
  // d.get_info<sycl::ext::intel::info::device::free_memory>();
  std::cout << "Total Device Mem (GB): " << totalMemory / 1024 / 1024 / 1024
            << std::endl;

  auto device_name = queue.get_device().get_info<sycl::info::device::name>();
  std::cout << "Running on " << device_name << "\n";

  bool copying = queue.get_device() != cpu_queue.get_device();

  float *h_a;
  float *h_b;
  float *h_c = sycl::malloc_host<float>(proj_len, cpu_queue);
  std::cout << "Generating data..." << std::endl;
  generateUniformCPU(h_a, h_b, proj_len, cpu_queue);
  std::cout << "Data generated." << std::endl;

  float *d_a = sycl::malloc_device<float>(proj_len, queue);
  float *d_b = sycl::malloc_device<float>(proj_len, queue);
  float *d_c = sycl::malloc_device<float>(proj_len, queue);

  sycl::range<1> gws(GWS(proj_len));
  sycl::range<1> lws(N_BLOCK_THREADS);

  sycl::event event{};
  std::vector<float> times(REPEAT, 0.0f);
  std::vector<float> times_transfer1(REPEAT, 0.0f);
  std::vector<float> times_transfer2(REPEAT, 0.0f);

  for (int i = 0; i < REPEAT; i++) {
    times_transfer1[i] = 0;
    if (copying) {
      event = queue.memcpy(d_a, h_a, proj_len * sizeof(float));
      times_transfer1[i] += wait_and_measure_time(event);
      queue.memcpy(d_b, h_b, proj_len * sizeof(float));
      times_transfer1[i] += wait_and_measure_time(event);
    } else {
      d_a = h_a;
      d_b = h_b;
    }

    event = queue.submit([&](sycl::handler &cgh) {
      cgh.parallel_for(sycl::nd_range<1>(gws, lws), [=](sycl::nd_item<1> item) {
        float items_a[N_ITEMS_PER_THREAD];
        float items_b[N_ITEMS_PER_THREAD];
        float items_c[N_ITEMS_PER_THREAD];

        const long long tile_offset = item.get_group(0) * TILE_ITEMS;
        const long long num_tiles = (proj_len + TILE_ITEMS - 1) / TILE_ITEMS;
        const long long num_tile_items = (item.get_group(0) == num_tiles - 1)
                                             ? proj_len - tile_offset
                                             : TILE_ITEMS;

        block_load<float, N_BLOCK_THREADS, N_ITEMS_PER_THREAD>(
            item.get_local_id(0), tile_offset, d_a, items_a, num_tile_items);
        block_load<float, N_BLOCK_THREADS, N_ITEMS_PER_THREAD>(
            item.get_local_id(0), tile_offset, d_b, items_b, num_tile_items);

#pragma unroll
        for (int i = 0; i < N_ITEMS_PER_THREAD; ++i) {
          if (item.get_local_id(0) + (N_BLOCK_THREADS * i) < num_tile_items) {
            items_c[i] = 2 * items_a[i] + 3 * items_b[i];
          }
        }

        block_store<float, N_BLOCK_THREADS, N_ITEMS_PER_THREAD>(
            item.get_local_id(0), tile_offset, d_c, items_c, num_tile_items);
      });
    });
    times[i] = wait_and_measure_time(event);
    if (copying) {
      event = queue.memcpy(h_c, d_c, proj_len * sizeof(float));
      times_transfer2[i] = wait_and_measure_time(event);
    } else {
      h_c = d_c;
    }
  }
  if (copying) {
    std::cout << "Data transfer times:" << std::endl;
    std::cout << " Copy to device: " << std::endl;
    print_mean_and_variance_of_time_cold_hot(times_transfer1);
    std::cout << " Copy to host: " << std::endl;
    print_mean_and_variance_of_time_cold_hot(times_transfer2);
  }
  std::cout << std::endl << "Kernel times:" << std::endl;
  auto stats_res = print_mean_and_variance_of_time_cold_hot(times);

  std::cout << "P,0," << proj_len << "," << REPEAT << "," << stats_res.cold
            << "," << stats_res.avg_hot << "," << stats_res.best_hot << ","
            << stats_res.stddev_hot << "," << COMPILER_NAME << ","
            << device_name << std::endl;

  // dump the benchmark result to a file:
  std::ofstream file;
  file.open("output.csv", std::ios_base::app);
  file << "P,0," << proj_len << "," << REPEAT << "," << stats_res.cold << ","
       << stats_res.avg_hot << "," << stats_res.best_hot << ","
       << stats_res.stddev_hot << "," << COMPILER_NAME << ","
       << device_name << std::endl;
  file.close();

  return 0;
}
