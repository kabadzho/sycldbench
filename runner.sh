#!/bin/bash

# Usage: ./runner.sh COMPILER [PATH_TO_COMPILER]

if [ $# -lt 1 ]; then
    echo "Usage: $0 COMPILER [PATH_TO_COMPILER]"
    exit 1
fi

COMPILER=$1
PATH_TO_COMPILER=${2:-/usr}

# Run the make command with the specified parameters
make COMPILER=$COMPILER PATH_TO_COMPILER=$PATH_TO_COMPILER projection
make COMPILER=$COMPILER PATH_TO_COMPILER=$PATH_TO_COMPILER join

for p_size in {1..10}
do
    # Calculate the projection/probe size
    SIZE=$((1<<(24 + p_size)))
    MEMORY_MB=$((SIZE * 12 / 1024 / 1024))
    echo "Benchmarks where each occupies <= $MEMORY_MB MB"
    echo "Running projection with size $SIZE"
    ./projection -p $SIZE > /dev/null

    for (( b_size=1; b_size<=p_size; b_size++ ))
    do
        # Calculate the build size
        BSIZE=$((1<<(24 + b_size)))
        echo "Running join with build size $BSIZE and probe size $SIZE"
        ./join -b $BSIZE -p $SIZE > /dev/null
    done
done

# Clean up
rm projection join
